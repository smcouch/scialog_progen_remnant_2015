\documentclass[11pt]{article}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{epstopdf}
\usepackage{wrapfig}
\usepackage{natbib}
%\usepackage[square,comma,numbers,sort]{natbib}
\usepackage[pdftex, plainpages=false, colorlinks=true, linkcolor=blue, citecolor=blue, bookmarks=false,urlcolor=blue]{hyperref}
\usepackage{setspace}
\usepackage{multicol}
\usepackage{sectsty}
\usepackage{url}
\usepackage{lipsum}
\usepackage{times}
\usepackage[tiny,compact]{titlesec}
\usepackage{fancyhdr}
\usepackage{deluxetable}
\usepackage[font=footnotesize,labelfont=bf]{caption}
\usepackage{verbatim}
\usepackage{acronym}
\usepackage[para]{footmisc}
\usepackage{comment}

\setlength{\textwidth}{6.5in}
\setlength{\oddsidemargin}{0.0cm}
\setlength{\evensidemargin}{0.0cm}
\setlength{\topmargin}{-0.5in}
\setlength{\headheight}{0.2in}
\setlength{\headsep}{0.2in}
\setlength{\textheight}{9.in}
%\setlength{\footskip}{-0.2in}
%\setlength{\voffset}{0.0in}


\sectionfont{\normalsize}
\subsectionfont{\normalsize}
\subsubsectionfont{\normalsize}
\singlespacing

\pagestyle{fancy}
\fancyhf{} 
\rhead{\fancyplain{}{Couch, Lopez, \& Zingale}}
\lhead{\fancyplain{}{Scialog: Supernova Progenitors and Remnants}}
\rfoot{\fancyplain{}{\thepage}}

\bibliographystyle{nsf}
%\bibliographystyle{hapj}
%\bibliographystyle{physrev}
\input journal_abbr.tex

%\titlespacing*{\section}{0in}{0.2in}{0in}
%\titlespacing*{\subsection}{0in}{0.1in}{0in}
\titleformat*{\subsection}{\itshape}
%\titlespacing*{\subsubsection}{0in}{0.in}{0in}
\titleformat*{\subsubsection}{\itshape}
\setlength{\abovecaptionskip}{3pt}

\begin{document}
\thispagestyle{plain}

\begin{center}
  {\bf \Large Connecting 3D Supernova Progenitors to Observed Remnants} \\
  {Sean M. Couch,\footnote{Michigan State University}
    Laura Lopez,\footnote{Ohio State University}
    \& Michael Zingale\footnote{SUNY Stony Brook}
  }
\end{center}


\begin{wrapfigure}[20]{r}{3.5in}
  \includegraphics[width=3.5in, clip]{supernova_progenitor_3D_velocity2.png}
  \caption{The radial velocity structure generated by vigorous silicon shell burning around the iron core of a massive star in the final minutes prior to gravitational core collapse \citep{Couch:2015a}.}
  \label{fig:progen}
\end{wrapfigure}

Real stars are not truly spherically-symmetric.
This is especially true for the interiors of massive stars at the end of their lives.
As massive stars approach core collapse, the equation of state becomes softer, cooling by neutrino emission drives nuclear burning ever more vigorously, and convective velocities increase. 
Nuclear burning couples to turbulent convection so that fuel is consumed in chaotic bursts. 
Core burning and thick shell burning are dominated by large scale modes of flow, which are of such low order that they do not cancel to a smooth spherical behavior.
The state-of-the art in core-collapse supernova (CCSN) progenitor models \citep[e.g.,][]{Woosley:2002, Woosley:2007d} is still 1D, and so simulations of the CCSN mechanism have all but exclusively used 1D initial conditions (ICs).
Recent exploration of the impact of multidimensional progenitor structure on the CCSN mechanism \citep{Couch:2015, Couch:2015a, Fernandez:2014, Muller:2015} has shown that non-spherical structure in progenitor models can have a qualitative and favorable impact on the CCSN mechanism.
What has not yet been explored is the impact of multidimensional progenitor structure on the character of the resulting supernova remnant.

In this project we will bridge the gap between supernova theory and observation by combining cutting edge computational and observational techniques to study the formation of supernova remnants.
To achieve this, we will carry out 3D simulations of massive stellar evolution, capturing the development of large scale convective burning in the star prior to its collapse and explosion.
We will then follow the explosion of this star all the way beyond the point of eruption from the stellar surface and directly simulate the asymmetric ejection of material into the circumstellar medium, i.e., the birth of a supernova remnant.
This will be the most realistic, physically-motivated prediction of the structure of a young supernova remnant yet.
We will use rigorous and world-leading observational methodology to compare the resulting prediction to real supernova remnants (SNRs) in the Galaxy.
We will also use the simulated remnant to suggest new observations to test the details and validity of our model.


We will simulate a select period in the evolution of a massive star in
3D.  The proof-of-principle work for doing 3D stellar evolution was
presented by \citet{Couch:2015a}.  Using the FLASH compressible
hydrodynamics code, they directly simulated the final growth of the
iron core from quasi-static burning to the self-consistent development
of gravitational instability and collapse once the core reached its
effective Chandrasekhar mass.  This spanned only the final three
minutes in the life of this massive star.  In the proposed work, we
will model the early evolution of the massive star shell burning and
convection with Maestro \citep{Nonaka:2010}, a low Mach number
hydrodynamics code tuned to modeling low speed flows for long
timescales.  We will use the same microphysics (EOS and reaction
network) and the late-stage simulations as in \citet{Couch:2015a} but
the use of the low-mach number approach will enable us to simulate far
long periods of time at comparable computational cost.  Using Maestro,
we will simulate the final hours in the life of massive supernova
progenitor during the period of simultaneous silicon shell burning and
vigorous oxygen-neon shell burning.  This will cover many convective
turnover times, establishing a quasi-steady state convective flow with
large, high-velocity convective eddies in both shells.  An example of
silicon shell burning in a massive star at the end of its life is
shown in Figure \ref{fig:progen} \citep[taken from][]{Couch:2015a}.

\begin{wrapfigure}[24]{l}{3.5in}
  \includegraphics[width=3.5in, clip]{casa_couch.pdf}
  \caption{Analysis of the symmetry of different elements (using power ratio analysis, a multipole expansion technique) observed in the young core-collapse SNR Cassiopeia~A. Quadrupole power ratio $P_{2}/P_{0}$ (a measure of ellipticity/elongation) vs. octupole power ratio $P_{3}/P_{0}$ (a measure of mirror asymmetry) for seven metals in Cas~A. The inset image shows the distribution of oxygen (green), silicon (red) and iron (blue). In Cas~A, the oxygen is the most circular, while the iron is the most elliptical and mirror asymmetric.}
  \label{fig:casa}
\end{wrapfigure}
Large scale aspherical convective motion in the precollapse progenitor can and {\it should} leave an imprint on the detailed structure of the remnant resulting from the supernova explosion.
To directly test this, with a physically consistent 3D progenitor structure in hand we will artificially drive mock supernova explosions in this star.
We will follow the development of this 3D explosion all the way beyond the point of shock eruption from the stellar surface and into the free expansion phase.
This will be accomplished using the flexibility allowed by adaptive mesh refinement techniques utilized previously by Couch for full-star supernova explosion simulations \citep[e.g.,][]{Couch:2009,Couch:2011}.


Following shock eruption, the model results (i.e., the density, velocity, and compositional structure) will be used as initial conditions to calculate the subsequent 3D hydrodynamical expansion of a SNR into the ISM/CSM and to predict the thermal emission produced by the hot shocked gas. 
We will techniques similar to those employed by \citet{Temim:2015} for 2D SNR simulations with idealized initial conditions.
We will compare these simulations directly to the relative morphologies and kinematics of metals in observations of young core-collapse SNRs using techniques discussed in \citet{Lopez:2014}. For example, we will measure the symmetry of many elements in nearby SNRs like Cassiopeia~A (see Figure~\ref{fig:casa}) and examine whether the results of our 3D simulations give similar results. 


Currently, nearly 300 SNRs have been identified and studied with multiwavelength observations in the Milky Way. We will capitalize on these data to compare the observed SNR properties to those predicted by the models as a powerful means to test and improve the simulations.
In turn, if we find our simulations to have explanatory power for the structure of certain young SNRs, we may be able to make predictions about SNRs based upon the simulations that could inform future SNR observing campaigns.

Brigding the gap between theory and observation has been a major theme of the Scialog in Time Domain Astrophysics.
For supernovae, doing so with light curves and spectra is difficult for a number of reasons including our lack of understanding of the details of the core-collapse supernova mechanism and the lack of a readily-available open-source high-fidelity radiative transfer code.
Additionally, radiative transfer necessarily integrates out much of the detailed structure of the supernova explosion.
Studying young core-collapse SNRs, however, is like performing an autopsy on the supernova, allowing us to pear into the guts of the explosion.
In this project we will, for the first time, simulate and explode a 3D supernova progenitor and follow its expansion and evolution into a young SNR.
We will then make direct connection to observations of young Galactic SNRs using cutting-edge observational analysis techniques, allowing us to directly address what are the important drivers in determining the shape and composition of SNRs and how do these relate to the character of the supernova explosion itself.

\newpage
%\renewcommand\bibsection{\section*{}}
\setlength{\bibsep}{2pt}
\bibliography{narrative}

\end{document}
